
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// CocoaLumberjack
#define COCOAPODS_POD_AVAILABLE_CocoaLumberjack
#define COCOAPODS_VERSION_MAJOR_CocoaLumberjack 1
#define COCOAPODS_VERSION_MINOR_CocoaLumberjack 9
#define COCOAPODS_VERSION_PATCH_CocoaLumberjack 1

// CocoaLumberjack/Core
#define COCOAPODS_POD_AVAILABLE_CocoaLumberjack_Core
#define COCOAPODS_VERSION_MAJOR_CocoaLumberjack_Core 1
#define COCOAPODS_VERSION_MINOR_CocoaLumberjack_Core 9
#define COCOAPODS_VERSION_PATCH_CocoaLumberjack_Core 1

// CocoaLumberjack/Extensions
#define COCOAPODS_POD_AVAILABLE_CocoaLumberjack_Extensions
#define COCOAPODS_VERSION_MAJOR_CocoaLumberjack_Extensions 1
#define COCOAPODS_VERSION_MINOR_CocoaLumberjack_Extensions 9
#define COCOAPODS_VERSION_PATCH_CocoaLumberjack_Extensions 1

// HexColors
#define COCOAPODS_POD_AVAILABLE_HexColors
#define COCOAPODS_VERSION_MAJOR_HexColors 2
#define COCOAPODS_VERSION_MINOR_HexColors 2
#define COCOAPODS_VERSION_PATCH_HexColors 1

// LBBlurredImage
#define COCOAPODS_POD_AVAILABLE_LBBlurredImage
#define COCOAPODS_VERSION_MAJOR_LBBlurredImage 0
#define COCOAPODS_VERSION_MINOR_LBBlurredImage 1
#define COCOAPODS_VERSION_PATCH_LBBlurredImage 0

// Mantle
#define COCOAPODS_POD_AVAILABLE_Mantle
#define COCOAPODS_VERSION_MAJOR_Mantle 1
#define COCOAPODS_VERSION_MINOR_Mantle 3
#define COCOAPODS_VERSION_PATCH_Mantle 1

// Mantle/extobjc
#define COCOAPODS_POD_AVAILABLE_Mantle_extobjc
#define COCOAPODS_VERSION_MAJOR_Mantle_extobjc 1
#define COCOAPODS_VERSION_MINOR_Mantle_extobjc 3
#define COCOAPODS_VERSION_PATCH_Mantle_extobjc 1

// ReactiveCocoa
#define COCOAPODS_POD_AVAILABLE_ReactiveCocoa
#define COCOAPODS_VERSION_MAJOR_ReactiveCocoa 2
#define COCOAPODS_VERSION_MINOR_ReactiveCocoa 2
#define COCOAPODS_VERSION_PATCH_ReactiveCocoa 3

// ReactiveCocoa/Core
#define COCOAPODS_POD_AVAILABLE_ReactiveCocoa_Core
#define COCOAPODS_VERSION_MAJOR_ReactiveCocoa_Core 2
#define COCOAPODS_VERSION_MINOR_ReactiveCocoa_Core 2
#define COCOAPODS_VERSION_PATCH_ReactiveCocoa_Core 3

// ReactiveCocoa/no-arc
#define COCOAPODS_POD_AVAILABLE_ReactiveCocoa_no_arc
#define COCOAPODS_VERSION_MAJOR_ReactiveCocoa_no_arc 2
#define COCOAPODS_VERSION_MINOR_ReactiveCocoa_no_arc 2
#define COCOAPODS_VERSION_PATCH_ReactiveCocoa_no_arc 3

// TSMessages
#define COCOAPODS_POD_AVAILABLE_TSMessages
#define COCOAPODS_VERSION_MAJOR_TSMessages 0
#define COCOAPODS_VERSION_MINOR_TSMessages 9
#define COCOAPODS_VERSION_PATCH_TSMessages 4

// UAProgressView
#define COCOAPODS_POD_AVAILABLE_UAProgressView
#define COCOAPODS_VERSION_MAJOR_UAProgressView 0
#define COCOAPODS_VERSION_MINOR_UAProgressView 1
#define COCOAPODS_VERSION_PATCH_UAProgressView 1

