//
//  AppDelegate.h
//  SimpleWeather
//
//  Created by Nadheer Chatharoo on 05/02/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
