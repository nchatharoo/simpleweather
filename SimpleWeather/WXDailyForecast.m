//
//  WXDailyForecast.m
//  SimpleWeather
//
//  Created by Nadheer Chatharoo on 25/06/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import "WXDailyForecast.h"

@implementation WXDailyForecast

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // 1
    NSMutableDictionary *paths = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    // 2
    paths[@"tempHigh"] = @"temp.max";
    paths[@"tempLow"] = @"temp.min";
    // 3
    return paths;
}

@end
