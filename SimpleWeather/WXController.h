//
//  WXViewController.h
//  
//
//  Created by Nadheer Chatharoo on 05/02/2014.
//
//

#import <UIKit/UIKit.h>

@interface WXController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@end
