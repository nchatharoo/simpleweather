//
//  WXClient.h
//  SimpleWeather
//
//  Created by Nadheer Chatharoo on 25/06/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <Foundation/Foundation.h>
//@import CoreLocation;
#import <CoreLocation/CoreLocation.h>
#import <ReactiveCocoa/ReactiveCocoa/ReactiveCocoa.h>

@interface WXClient : NSObject

//@import Foundation;
- (RACSignal *)fetchJSONFromURL:(NSURL *)url;
- (RACSignal *)fetchCurrentConditionsForLocation:(CLLocationCoordinate2D)coordinate;
- (RACSignal *)fetchHourlyForecastForLocation:(CLLocationCoordinate2D)coordinate;
- (RACSignal *)fetchDailyForecastForLocation:(CLLocationCoordinate2D)coordinate;

@end
